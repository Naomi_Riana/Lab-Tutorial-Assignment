$(document).ready(function() {

    // kode jQuery selanjutnya akan ditulis disini
    var themes = [
        {"id": 0, "text": "Red", "bcgColor": "#F44336", "fontColor": "#FAFAFA"},
        {"id": 0, "text": "Pink", "bcgColor": "#E91E63", "fontColor": "#FAFAFA"},
        {"id": 0, "text": "Purple", "bcgColor": "#9C27B0", "fontColor": "#FAFAFA"},
        {"id": 0, "text": "Indigo", "bcgColor": "#3F51B5", "fontColor": "#FAFAFA"},
        {"id": 0, "text": "Blue", "bcgColor": "#2196F3", "fontColor": "#212121"},
        {"id": 0, "text": "Teal", "bcgColor": "#009688", "fontColor": "#212121"},
        {"id": 0, "text": "Lime", "bcgColor": "#CDDC39", "fontColor": "#212121"},
        {"id": 0, "text": "Yellow", "bcgColor": "#FFEB3B", "fontColor": "#212121"},
        {"id": 0, "text": "Amber", "bcgColor": "#FFC107", "fontColor": "#212121"},
        {"id": 0, "text": "Orange", "bcgColor": "#FF5722", "fontColor": "#212121"},
        {"id": 0, "text": "Brown", "bcgColor": "#795548", "fontColor": "#FAFAFA"}
    ];

    var selectedtheme = {"Indigo": {"bcgColor": "#3F51B5", "fontColor": "#FAFAFA"}};

    $('body').css("background", selectedtheme.Indigo.bcgColor);
    $('h1').css("color", selectedtheme.Indigo.fontColor);
    $('.apply-button').css("background-color", selectedtheme.Indigo.bcgColor);
    $('.apply-button').css("color", selectedtheme.Indigo.fontColor);

    $('.my-select').select2({
        'data': themes
    });

    $('.apply-button').on('click', function () {  // sesuaikan class button
        var themesthemes = $('.my-select').select2('data');
        var colortheme = themesthemes[0].text;

        var selectedthe = {};
        selectedthe.colortheme = {
            'bcgColor': themesthemes[0].bcgColor,
            'fontColor': themesthemes[0].fontColor
        };

        $('body').css("background", selectedthe.colortheme.bcgColor);
        $('h1').css("color", selectedthe.colortheme.fontColor);
        $('.apply-button').css("background-color", selectedthe.colortheme.bcgColor);
        $('.apply-button').css("color", selectedthe.colortheme.fontColor);

        selectedtheme[colortheme] = selectedthe[colortheme];
    })

    $(".chat-text textarea").keypress(function (event) {
        if (event.keyCode === 13) {
            var message = $('textarea').val();
            $('.chat >  .message:first')
                .clone()
                .text(message)
                .appendTo('.chat');
            document.getElementById('text-chat').value = '';
        }
    })
})

var erase = false;
var go = function(x) {
	var print = document.getElementById('print');

  if (x === 'ac') {
    /* implemetnasi clear all */
    print.value = "";
    erase = false;
  } else if (x === 'eval') {
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;
  }else if(x==='log'||x==='sin'||x==='tan'){

  		if(x==='sin'||x==='tan'){print.value = eval('Math.'+x+'('+toDegrees(print.value)+')').toPrecision(2);}else{
  		print.value = eval('Math.'+x+'10'+'('+print.value+')').toPrecision(2);}
  		erase = true;

  } else {
    //jika erase true, hapus text di kalkulator untuk kalkulasi angka baru
  	if(erase===true&&(x!==' * '&&x!==' - '&&x!==' + '&&x!==' / ')){
    	print.value = "";
	}
		print.value += x;
		erase = false;
  }
};

//konversi sudut radian ke derajat
function toDegrees (angle) {
  return angle * Math.PI/180;
}

function evil(fn) {
  return new Function('return ' + fn)();
}
// END

