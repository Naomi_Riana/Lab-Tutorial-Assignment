from __future__ import unicode_literals
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
from .api_enterkomputer import get_drones, get_soundcard, get_optical

# Create your views here.
response = {}
response['author'] = "Naomi"

# fungsi ini untuk render tampilan login/belum login
# kalau sudah login, bakal redirect ke halaman profil
# kalau belum login, bakal ditampilkan halaman login
def index(request):
    print("#==> masuk index")
    if  'user_login' in request.session:
        return HttpResponseRedirect(reverse('lab-9:profile')) # halaman profil
    else:
        html = 'lab_9/session/login.html' # halaman login
        return render(request, html, response)

# fungsi ini untuk menyimpan username, id, token akses ke dalam response
# beserta daftar drone, soundcard, dan optical
# sama memuat list favorite drone, soundcard, optical
def set_data_for_session(res, request):
    response['author'] = request.session['user_login']
    response['access_token'] = request.session['access_token']
    response['kode_identitas'] = request.session['kode_identitas']
    response['role'] = request.session['role']
    response['drones'] = get_drones().json()
    response['soundcard'] = get_soundcard().json()
    response['optical'] = get_optical().json()

    # print ("#drones = ", get_drones().json(), " - response = ", response['drones'])
    ## handling agar tidak error saat pertama kali login (session kosong)
    if  'drones' in request.session.keys():
        response['fav_drones'] = request.session['drones']

        # jika tidak ditambahkan else, cache akan tetap menyimpan data
        # sebelumnya yang ada pada response, sehingga data tidak up-to-date
    else:
        response['fav_drones'] = []

    if 'soundcard' in request.session.keys():
        response['fav_soundcard'] = request.session['soundcard']
    else:
        response['fav_soundcard'] = []

    if 'optical' in request.session.keys():
        response['fav_optical'] = request.session['optical']
    else:
        response['fav_optical'] = []

# fungsi untuk menampilkan halaman profile
# kalau udah login nanti halaman profile dirender
# kalau ga, balik lagi ke index (halaman login)
def profile(request):
    print("#==> profile")
    if  'user_login' not in request.session.keys():
        return HttpResponseRedirect(reverse('lab-9:index'))

    set_data_for_session(response, request) #menyiapkan data user login yada yada yada

    html = 'lab_9/session/profile.html'
    return render(request, html, response)

# fungsi untuk menambahkan favorit drones
def add_session_drones(request, id):
    ssn_key = request.session.keys()
    if not 'drones' in ssn_key:
        print("# init drones ")
        request.session['drones'] = [id]

    else:
        drones = request.session['drones']
        print("# existing drones => ", drones)
        if  id not in drones:
            print("# add new item, then save to session")
            drones.append(id)
            request.session['drones'] = drones #list drones setelah add drone

    messages.success(request, "Berhasil tambah drone favorite")
    return HttpResponseRedirect(reverse('lab-9:profile'))

# fungsi untuk menghapus favorit soundcard
def del_session_drones(request, id):
    print("# DEL drones")
    drones = request.session['drones']
    print("before = ", drones)
    drones.remove(id) #dihapus
    request.session['drones'] = drones #list fav drones setelah drone dihapus
    print("after = ", drones)

    messages.error(request, "Berhasil hapus dari favorite")
    return HttpResponseRedirect(reverse('lab-9:profile'))

# mereset favorite drones
def clear_session_drones(request):
    print ("# CLEAR session drones")
    print ("before 1 = ", request.session['drones'])
    del request.session['drones'] #listnya dihapus

    messages.error(request, "Berhasil reset favorite drones")
    return HttpResponseRedirect(reverse('lab-9:profile'))

# fungsi untuk menambahkan favorit soundcard
def add_session_soundcard(request, id):
    ssn_key = request.session.keys()
    if not 'soundcard' in ssn_key:
        print("# init sound card ")
        request.session['soundcard'] = [id]

    else:
        soundcard = request.session['soundcard']
        print("# existing sound card => ", soundcard)
        if  id not in soundcard:
            print("# add new item, then save to session")
            soundcard.append(id)
            request.session['soundcard'] = soundcard #list soundcard yang sudah diupdate

    messages.success(request, "Berhasil tambah sound card favorite")
    return HttpResponseRedirect(reverse('lab-9:profile'))

# fungsi untuk menghapus favorite soundcard
def del_session_soundcard(request, id):
    print("# DEL sound card")
    soundcard = request.session['soundcard']
    print("before = ", soundcard)
    soundcard.remove(id)
    request.session['soundcard'] =  soundcard #list soundcard yang sudah diupdate
    print("after = ", soundcard)

    messages.error(request, "Berhasil hapus sound card dari favorite")
    return HttpResponseRedirect(reverse('lab-9:profile'))

# fungsi untuk mereset soundcard
def clear_session_soundcard(request):
    print ("# CLEAR session sound card")
    print ("before 1 = ", request.session['soundcard'])
    del request.session['soundcard'] #list fave soundcard dihapus

    messages.error(request, "Berhasil reset favorite sound card")
    return HttpResponseRedirect(reverse('lab-9:profile'))

#fungsi untuk menambahkan fave optical
def add_session_optical(request, id):
    ssn_key = request.session.keys()
    if not 'optical' in ssn_key:
        print("# init optical ")
        request.session['optical'] = [id]

    else:
        optical = request.session['optical']
        print("# existing sound card => ", optical)
        if  id not in optical:
            print("# add new item, then save to session")
            optical.append(id)
            request.session['optical'] = optical #list optical yang sudah diupdate

    messages.success(request, "Berhasil tambah optical favorite")
    return HttpResponseRedirect(reverse('lab-9:profile'))

# fungsi untuk menghapus fave optical
def del_session_optical(request, id):
    print("# DEL optical")
    optical = request.session['optical']
    print("before = ", optical)
    optical.remove(id)
    request.session['optical'] = optical
    print("after = ", optical)

    messages.error(request, "Berhasil hapus optical dari favorite")
    return HttpResponseRedirect(reverse('lab-9:profile'))

# fungsi untuk mereset fave optical
def clear_session_optical(request):
    print ("# CLEAR session optical")
    print ("before 1 = ", request.session['optical'])
    del request.session['optical'] #list dihapus

    messages.error(request, "Berhasil reset favorite optical")
    return HttpResponseRedirect(reverse('lab-9:profile'))

# fungsi untuk login
# kalau sudah login, masuk ke halaman profile
# kalau ga kembali ke halaman login
def cookie_login(request):
    print("#==> masuk login")
    if  is_login(request):
        return HttpResponseRedirect(reverse('lab-9:cookie_profile'))
    else:
        html = 'lab_9/cookie/login.html'
        return render(request, html, response)

# fungsi dijalankan pas udh login
# dicek apakah sesuai dengan username + password di my_cookie_auth
# kalau username/password salah nanti kembali ke halaman login
# terus ada message error
def cookie_auth_login(request):
    print("# Auth login")
    if request.method == "POST":
        user_login = request.POST['username']
        user_password = request.POST['password']

        if my_cookie_auth(user_login, user_password):
            print("#SET cookies")
            res = HttpResponseRedirect(reverse('lab-9:cookie_login'))

            res.set_cookie('user_login', user_login)
            res.set_cookie('user_password', user_password)

            return res

        else:
            msg = "Username atau password salah"
            messages.error(request, msg)
            return HttpResponseRedirect(reverse('lab-9:cookie_login'))

    else:
        return HttpResponseRedirect(reverse('lab-9:cookie_login'))

# fungsi untuk menampilkan profile cookie
# kalau belum login, ya...redirect ke halaman login
# kalau ga, username + password bakal disimpan di dalam cookie
# terus dicek apakah sesuai dengan username + password di my_cookie_auth
def cookie_profile(request):
    print("# cookie profile ")

    if not is_login(request):
        print("belum login")
        return HttpResponseRedirect(reverse('lab-9:cookie_login'))

    else:
        #print ("cookies => ", request.COOKIES)
        in_uname = request.COOKIES['user_login']
        in_pwd = request.COOKIES['user_password']

        if my_cookie_auth(in_uname, in_pwd):
            html = "lab_9/cookie/profile.html"
            res = render(request, html, response)
            return  res

        else:
            print("#login dulu")
            msg = "Kamu tidak punya akses h3h3 :P"
            messages.error(request, msg)
            html = "lab_9/cookie/login.html"
            return render(request, html, response)

#fungsi untuk mereset cookie aka logout hehe.....
def cookie_clear(request):
    res = HttpResponseRedirect('/lab-9/cookie/login')
    res.delete_cookie('lang')
    res.delete_cookie('user_login')

    msg = "Anda berhasil logout. Cookies direset"
    messages.info(request, msg)
    return res

# nanti buat mengecek apakah kita login dengan nama
# DIO dan password zawarudowry
def my_cookie_auth(in_uname, in_pwd):
    my_uname = "DIO"
    my_pwd = "zawarudowry"
    return  in_uname == my_uname and in_pwd == my_pwd

# mengecek apakah kita sedang login atau tidak
def is_login(request):
    return 'user_login' in request.COOKIES and 'user_password' in request.COOKIES