import requests

DRONE_API = 'https://www.enterkomputer.com/api/product/drone.json'
SOUNDCARD_API = 'https://www.enterkomputer.com/api/product/soundcard.json'
OPTICAL_API = 'https://www.enterkomputer.com/api/product/optical.json'

def get_drones():
    drones = requests.get(DRONE_API)
    return drones

def get_soundcard():
    sound_card = requests.get(SOUNDCARD_API)
    return sound_card

def get_optical():
    optical = requests.get(OPTICAL_API)
    return optical