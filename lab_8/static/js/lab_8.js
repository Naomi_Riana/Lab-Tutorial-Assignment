// FB initiation function
  window.fbAsyncInit = () => {
    FB.init({
      appId      : '146788682712739',
      cookie     : true,
      xfbml      : true,
      version    : 'v2.11'
    });

    // implementasilah sebuah fungsi yang melakukan cek status login (getLoginStatus)
    // dan jalankanlah fungsi render di bawah, dengan parameter true jika
    // status login terkoneksi (connected)
    // Hal ini dilakukan agar ketika web dibuka dan ternyata sudah login, maka secara
    // otomatis akan ditampilkan view sudah login
FB.getLoginStatus(function(response) {
    statusChangeCallback(response);
});
  };

  // Call init facebook. default dari facebook
  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));

  function statusChangeCallback(response){
      if (response.status === 'connected'){
          console.log("Masuk");
          render(true);
      }
      else {console.log("Tidak masuk")}
  }

  // Fungsi Render, menerima parameter loginFlag yang menentukan apakah harus
  // merender atau membuat tampilan html untuk yang sudah login atau belum
  // Ubah metode ini seperlunya jika kalian perlu mengganti tampilan dengan memberi
  // Class-Class Bootstrap atau CSS yang anda implementasi sendiri
    const render = loginFlag => {
        if (loginFlag) {
            $('#Login').hide();
            // Jika yang akan dirender adalah tampilan sudah login
            // Panggil Method getUserData yang anda implementasi dengan fungsi callback
            // yang menerima object user sebagai parameter.
            // Object user ini merupakan object hasil response dari pemanggilan API Facebook.
            getUserData(user => {
                // Render tampilan profil, form input post, tombol post status, dan tombol logout
                $('#lab8').html(
                    '<div class="home-page" style="margin-left: 100px">' +
                    '<div class="header_section">' +
                    '<div class="cover_section">' +
                    '<img class="cover" style="filter: brightness(50%);height: 312px; width: 820.114px;" src="' + user.cover.source + '" alt="cover" />' +'<br><br>'+
                    '<div class="name" style="top: 50px; left:270px; position:absolute; ">' + '<h1>' + '<span class="nama">' +
                    '<a style="color: white; font-family: Montserrat;text-align: center;font-size: 60px" href="https://www.facebook.com/naomi.r.lg">' + user.name + '</a>' +
                    '</span>' + '</h1>' +
                     '<button style="    width: 170px;\n' +
                    ' background-color: #3b5998;\n' +
                    '    font-family: Montserrat;\n' +
                    '    color: white;\n' +
                    '    border: none; "onclick="facebookLogout()">' + "Logout" + '</button>'  +
                    '</div>' +
                    '</div>' +
                    '<div class="about_section" style="background-color: white;width:820.114px">' +
                    '<div class="photo_profile_cont">' +
                    '<img style="height:170px;width=170px;" class="picture" src="' + user.picture.data.url + '" alt="profpic" />' +
                    '<span class="profile" style="margin-left:30px;margin-top:50px;position: absolute;">' +
                    '<p style="font-size:28px;font-family: Montserrat;color: #3b5998">' + user.gender+ '    -    ' + user.email+ '</p>' +
                    '</span>' +
                    '</div>' +
                    '</div>' +
                    '</div>' + '<br>' + '<p style="width:820.114px;background-color:white; font-size:28px;font-family: Montserrat;color: #3b5998">' +
                    'Status' + '</p>' +
                    '<input id="postInput" style="height: 200px; width: 820.114px" type="text" class="post" placeholder="What\'s on your mind?" />' +
                    '<br><br>' + '<button style="font-family: Montserrat;font-size:20px;border-radius:10px;width:150px;border: none; background-color: white;color: #3b5998;"class="postStatus" onclick="postStatus()">Kirim</button>' +
                    '<br><br>' +'<div id="content_container" style="border-radius:20px;width:820.114px;background-color: white">' +
                    '<p style="margin-left:30px;margin-top:20px;font-size:28px;font-family: Montserrat;color: #3b5998">' +
                    'Timeline' + '</p>' + '<hr style="border-color: #bdc3c7">' +
                    '</div>' +
                    '</div>'
                );
            });
            // Setelah merender tampilan di atas, dapatkan data home feed dari akun yang login
            // dengan memanggil method getUserFeed yang kalian implementasi sendiri.
            // Method itu harus menerima parameter berupa fungsi callback, dimana fungsi callback
            // ini akan menerima parameter object feed yang merupakan response dari pemanggilan API Facebook
            getUserFeed(feed => {
                feed.data.map(value => {
                    // Render feed, kustomisasi sesuai kebutuhan.
                    if (value.message && value.story) {
                        $('#content_container').append(
                            '<div class="feed" style="margin-left:30px;">' +
                            '<p style="font-size:16px" >' + value.message + '</p>' +
                            '<p style="color:#3b5998;font-size:14px; font-weight: bold">' + value.story + '</p>' +
                            '</div>' + '<hr>'
                        );
                    } else if (value.message) {
                        $('#content_container').append(
                            '<div class="feed" style="margin-left:30px;background-color:white;">' +
                            '<p style="font-size:16px">' + value.message + '</p>' +
                            '</div>' + '<hr>'
                        );
                    } else if (value.story) {
                        $('#content_container').append(
                            '<div class="feed" style="margin-left:30px;background-color: white;">' +
                            '<p style="width:820.114px;color:#3b5998;font-size:14px; font-weight: bold">' + value.story + '</p>' +
                            '</div>' + '<hr>'
                        );
                    }
                });
            });

        }

        else {
            // Tampilan ketika belum login
            $('#lab8').hide();
            $('#Login').show();
            console.log("Tidak masuk");
        }

    }

        const facebookLogin = () => {
            // TODO: Implement Method Ini
            // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan sudah login
            // ketika login sukses, serta juga fungsi ini memiliki segala permission yang dibutuhkan
            // pada scope yang ada. Anda dapat memodifikasi fungsi facebookLogin di atas.
            FB.login(function (response) {
                console.log(response);
                $('#Login').hide()
                render(response.status === 'connected');
            }, {scope: 'public_profile, user_posts,publish_actions,email,user_about_me' })
            // Dengan menambahkan permission user_posts dan publish_actions, 
            // apa saja yang dapat dilakukan oleh aplikasi kita menggunakan Graph API?
            // ANSWER : nanti aplikasi bisa dapat ijin untuk akses info mengenai postingan/status, aktivitas kita, email, serta bio kita ;)
        };


        const facebookLogout = () => {
            // TODO: Implement Method Ini
            // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan belum login
            // ketika logout sukses. Anda dapat memodifikasi fungsi facebookLogout di atas.
            FB.getLoginStatus(function (response) {
                if (response.status === 'connected') {
                    FB.logout();
                    render(false);
                    console.log("Keluar")
                }
            });

        };

        // TODO: Lengkapi Method Ini
        // Method ini memodifikasi method getUserData di atas yang menerima fungsi callback bernama fun
        // lalu merequest data user dari akun yang sedang login dengan semua fields yang dibutuhkan di
        // method render, dan memanggil fungsi callback tersebut setelah selesai melakukan request dan
        // meneruskan response yang didapat ke fungsi callback tersebut
        // Apakah yang dimaksud dengan fungsi callback?
        // ANSWER : fungsi yang parameternya adalah fungsi lainnya
        const getUserData = (getUserData) => {
            FB.getLoginStatus(function (response) {
                if (response.status === 'connected') {
                    FB.api('/me?fields=id,name,cover,picture.type(large),about,email,gender', 'GET', function (response) {
                        console.log(response);
                        getUserData(response)
                    });
                }
            });
        };

          const getUserFeed = (getUserFeed) => {
            // TODO: Implement Method Ini
            // Pastikan method ini menerima parameter berupa fungsi callback, lalu merequest data Home Feed dari akun
            // yang sedang login dengan semua fields yang dibutuhkan di method render, dan memanggil fungsi callback
            // tersebut setelah selesai melakukan request dan meneruskan response yang didapat ke fungsi callback
            // tersebut

            FB.getLoginStatus(function(response) {
                if (response.status === 'connected') {
                    FB.api('/me/feed?fields=story,message,name,created_time', 'GET', function(response){
                        console.log(response);
                        getUserFeed(response);
                    });
                }
            });
          };

      const postFeed = (status) => {
        // Todo: Implement method ini,
        // Pastikan method ini menerima parameter berupa string message dan melakukan Request POST ke Feed
        // Melalui API Facebook dengan message yang diterima dari parameter.
         var message = status;
         FB.api('/me/feed', 'POST', {message:message});
         console.log(message);
         render(true);
      };

        const postStatus = () => {
            const message = $('#postInput').val();
            postFeed(message);
        };
